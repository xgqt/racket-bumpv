;; This file is part of racket-bumpv - Bump your Racket-based project version.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-bumpv is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-bumpv is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-bumpv.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (only-in scribble/bnf nonterm)
          bumpv/cli
          bumpv/support)


@title[#:tag "bumpv-cli"]{Command-line interface}


@section[#:tag "bumpv-cli-synopsis"]{Synopsis}

BumpV invocation in any one of the following forms is accepted:

@itemlist[
 @item{@exec{bumpv [option-flag] ... <MAJOR> <MINOR> <PATCH>}}
 @item{@exec{bumpv <info-flag>}}
 ]


@section[#:tag "bumpv-cli-option-flags"]{Option flags}

The @exec{option-flag} is any one of the following:

@itemlist[
 @item{
  @Flag{n} @nonterm{file-name} or @DFlag{name} @nonterm{file-name}
  --- use @nonterm{file-name} as the version file name.

  Defaults to @code{@format["~v" (version-file-name)]}.
 }
 @item{
  @Flag{r} @nonterm{file-regexp} or @DFlag{regexp} @nonterm{file-regexp}
  --- use @nonterm{file-regexp} as the target files @racket[regexp].

  Defaults to @code{@format["~v" (target-files-regexp)]}.
 }
 @item{
  @Flag{P} or @DFlag{no-patch-0s}
  --- exclude PATCH equal to 0 from version string.

  This means that if the @racketid[PATCH] version component
  is equal to @racket[0],
  then when a new target file package version definition is written
  (see @DFlag{regexp} flag),
  a git commit (see @DFlag{commit} flag)
  or a git tag is created (see @DFlag{tag} flag),
  the version string will consist
  only of @racketid[MAJOR] and @racketid[MINOR] version components,
  eg @racket{1.1} instead of @racket{1.1.0}.

  Defaults to @code{@format["~v" (not (allow-PATCH-0s))]}
  (@racket[0]s are allowed).
 }
 @item{
  @Flag{c} or @DFlag{commit}
  --- create a git commit.

  Defaults to @code{@format["~v" (git-create-commit?)]}.
 }
 @item{
  @Flag{t} or @DFlag{tag}
  --- create a git tag.

  Defaults to @code{@format["~v" (git-create-tag?)]}.
 }
 @item{
  @Flag{q} or @DFlag{quiet}
  --- be quiet (minimal/no console output).

  Defaults to @code{@format["~v" (not (verbose?))]}.
 }
 @item{
  @Flag{v} or @DFlag{verbose}
  --- be verbose (detailed console output).

  Defaults to @code{@format["~v" (verbose?)]}.
 }
 ]


@section[#:tag "bumpv-cli-info-flags"]{Information flags}

The @exec{info-flag} is any one of the following:

@itemlist[
 @item{
  @Flag{s} or @DFlag{show}
  --- show current version components.
 }
 @item{
  @Flag{h} or @DFlag{help}
  --- show the help page.
 }
 @item{
  @Flag{V} or @DFlag{version}
  --- show the version of this program and exit.
 }
 ]
