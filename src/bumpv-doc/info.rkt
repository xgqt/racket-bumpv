#lang info


(define pkg-desc "Bump your Racket-based project version. Documentation.")

(define version "1.2.1")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"))

(define build-deps
  '("racket-doc"
    "scribble-lib"
    "ziptie-git"
    "bumpv-lib"))
