# BumpV

<p align="center">
  <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/racket-bumpv">
    <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/racket-bumpv/">
  </a>
  <a href="https://gitlab.com/xgqt/racket-bumpv/pipelines">
    <img src="https://gitlab.com/xgqt/racket-bumpv/badges/master/pipeline.svg">
  </a>
</p>

Quickly bump the version of your Racket-based projects.


## About

BumpV is a tool that helps to quickly make new version releases of your
Racket-based projects.

BumpV does not need any special config files (unlike Req for example)
but it needs some special Racket code inside one of your project's packages.

You will have to create a file which defines
`MAJOR`, `MINOR` and `PATCH`
variables, the so-called version components.

For example:

```racket
(provide (all-defined-out))

(define-values (MAJOR MINOR PATCH)
  (values 1 2 3))
```

The most important line in this file is the one with `values`.
BumpV only wants this line to be present, mostly because it does not do any
advanced file parsing, just simple regular expression matching.

By running BumpV:

- contents of file containing version component definitions will be replaced
  by the new version components that were specified on the command line,
- all strings, in info.rkt or specified files,
  of the form `<MAJOR>.<MINOR>.<PATCH>`,
  where the components are the old version components,
  will be replaced with strings with the new version components,
- optionally a git commit and a tag will be created,
  message in both will be: `bump to <MAJOR>.<MINOR>.<PATCH>`
  (with new version components).


## Installation

### Make

Use GNU Make to install BumpV from its project directory.

```sh
make install
```

### Raco

Use raco to install BumpV from the official Racket Package Catalog.

```sh
raco pkg install --auto --skip-installed --user bumpv
```

### Req

Use Req to install BumpV from its project directory.

``` sh
raco req --everything --verbose
```


## Documentation

Documentation can either be built locally with `make public`
or browsed online on either
[GitLab pages](https://xgqt.gitlab.io/racket-bumpv/)
or
[Racket-Lang Docs](https://docs.racket-lang.org/bumpv/).


## License

Copyright (c) 2022, Maciej Barć xgqt@riseup.net

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
