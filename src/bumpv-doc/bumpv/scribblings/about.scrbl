;; This file is part of racket-bumpv - Bump your Racket-based project version.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-bumpv is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-bumpv is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-bumpv.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket/base))


@title[#:tag "bumpv-about"]{About}


BumpV is a tool that helps to quickly make new version releases of your
Racket-based projects.

BumpV does not need any special config files (unlike Req for example)
but it needs some special Racket code inside one of your project's packages.

You will have to create a file which defines
@racketid[MAJOR], @racketid[MINOR] and @racketid[PATCH]
variables, the so-called version components.

For example:

@racketblock[
(provide (all-defined-out))

(define-values (MAJOR MINOR PATCH)
  (values 1 2 3))
]

The most important line in this file is the one with @racket[values].
BumpV only wants this line to be present, mostly because it does not do any
advanced file parsing, just simple regular expression matching.

By running BumpV:

@itemlist[
 @item{
  contents of file containing version component definitions will be replaced
  by the new version components that were specified on the command line,
 }
 @item{
  all strings, in @filepath{info.rkt} or specified files,
  of the form @racket{MAJOR.MINOR.PATCH},
  where the components are the old version components,
  will be replaced with strings with the new version components,
 }
 @item{
  optionally a @exec{git} commit and a tag will be created,
  message in both will be: @racket{bump to MAJOR.MINOR.PATCH}
  (with new version components).
 }
 ]
